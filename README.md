**Serpent's Proxy Block**
This project is the code used in the website/API I will be hosting, compiling a database of free proxies found online for the purpose of banning them

**How to set up**
Import *entries.sql* to your database, and put the credentials in *sql_auth.example.php* and adjust the path in the include statement *(Line X)*.