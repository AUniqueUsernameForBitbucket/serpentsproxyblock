import zipfile
from io import BytesIO

import requests
from bs4 import BeautifulSoup

PROXIES = set()
SESSION = requests.Session()


def fetch_proxies(session: requests.Session, url: str, keyword: str, file_names: list):
    """
    Scrape proxies from websites listed on http://bestproxysites24.blogspot.com/

    Given a session, scrape from a url listed on that website based on the keywords
    given in the title of the post. Use url of website, distinct proxy titles, and filenames the proxies
    are located in order to retrieve all the proxies.

    Writes a file named proxies.txt in this files directory
    """

    response = session.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    proxy_links = []
    download_links = []

    for tag in soup.find_all('a', href=True):
            if keyword in tag.get_text():
                proxy_links.append(tag['href'])

    for link in proxy_links:
        response = session.get(link)
        soup = BeautifulSoup(response.text, 'html.parser')
        for tag in soup.find_all('a', href=True):
                if tag['href'].startswith('https://drive.google.com'):
                    download_links.append(tag['href'])

    for link in download_links:
        response = session.get(link)
        with zipfile.ZipFile(BytesIO(response.content)) as z:
            for file in file_names:
                with z.open(file) as fp:
                    PROXIES.update({proxy.decode().strip() for proxy in fp})

    print(f'Scraped proxies from {url}')


data = [
    ('http://www.sslproxies24.top/', 'Free SSL Proxies', ['ssl.txt']),
    ('http://www.freshnewproxies24.top/', 'Fresh New Proxies', ['l1.txt', 'l2.txt', 'l3.txt'])
]

for site, kw, filename in data:
    fetch_proxies(SESSION, site, kw, filename)

with open('proxies.txt', 'w') as fp:
    fp.write('\n'.join(PROXIES))
