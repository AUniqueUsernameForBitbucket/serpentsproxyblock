<?php
include('../sql_auth.php');
$starttime = microtime(true);
$proxylist = explode("\n", file_get_contents('proxies.txt'));
$newadded = 0;
$skipped = 0;

$conn = mysqli_connect(SQL_HOST, SQL_USER, SQL_PASS, SQL_NAME);
if(!$conn) { die("Connection failed: " . mysqli_connect_error()); }

foreach($proxylist as $k => $c) {
	if($c==''||$c==' '||$c==':'||$c[0]=='#') { continue; }
	$c = explode(':', $c);
	$sql = "SELECT `ip`, `port` FROM `entries` WHERE `ip`='".$c[0]."'";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result)== 0) { 
		$newadded++;
		$sql = "INSERT INTO `entries` (`ip`, `port`) VALUES ('".$c[0]."', '".$c[1]."')";
		mysqli_query($conn, $sql);
	} else {
		$skipped++;
	}
}
mysqli_close($conn);
echo 'Newly Added: '.$newadded.PHP_EOL;
echo 'Skipped Old: '.$skipped.PHP_EOL;
$endtime = microtime(true);
$timediff = round($endtime - $starttime);
echo 'Time Elapsed: '.$timediff.' seconds'.PHP_EOL;
file_put_contents('proxies.txt', '#Cleared '.date("Y-m-d H:i:s"));
file_put_contents('log.txt', 'Updated '.date("Y-m-d H:i:s").' ,'.$newadded.' new, '.$skipped.' old, in '.$timediff.' seconds.'.PHP_EOL, FILE_APPEND);
?>
